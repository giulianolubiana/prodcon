# ProdCon
This is a simple application that implements a producer consumer
pattern.

The  producer generates stock values for a company, the consumer prints
the values on screen.

The application has been successfully compiled with *clang* and *gcc*.

## Implementation
The buffer used by the threads to communicate has been wrapped inside
a synchronization class to manage the correct accesses to it
[synchronization_queue.h](https://bitbucket.org/giulianolubiana/prodcon/src/master/src/src/syncronized_queue.h).

The "Producer" and the "Consumer" classes are abstract so you can
easily extend them to your needs. So each of them has a concrete
version that implements the specificities of the application.

## Test
You can find the executable to run all the tests in:

    /src/test/alltests

## Executable
The main executable is located in:

    /src/main
