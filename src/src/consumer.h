#ifndef CONSUMER_H_
#define CONSUMER_H_

#include "syncronized_queue.h"
#include "utility.h"

template <typename T, typename ChannelT = syncronized_queue<T>>
class consumer {
 public:
  consumer(syncronized_queue<T>& queue) : queue_{queue}{}

  void operator()() {
    T item = queue_.get();
    consume(item);
  }

  virtual ~consumer() = default;

 protected:
  virtual void consume(T& x) = 0;
  void send(const T& t) {
    queue_.put(t);
  }
  T s;

 private:
  syncronized_queue<T>& queue_;
  bool stop = false;
};

#endif // CONSUMER_H_
