#ifndef PRODUCER_H_
#define PRODUCER_H_

#include "syncronized_queue.h"
#include "utility.h"
#include <string>
#include <thread>
#include <iostream>

template <typename T, typename ChannelT = syncronized_queue<T>>
class producer {
 public:
  producer(ChannelT& queue) : queue_{queue} {}

  void operator()() {
    T o = produce();
    queue_.put(o);
  }

  virtual ~producer() = default;

 protected:
  ChannelT& queue_;

  virtual T produce() = 0;
  T read() {
    return queue_.get();
  }
};
#endif //PRODUCER_H_
