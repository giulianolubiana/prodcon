#ifndef SYNC_QUEUE_H_
#define SYNC_QUEUE_H_

#include <mutex>
#include <list>
#include <condition_variable>

/*
  A syncronized wrapper aroud a generic buffer
 */
template <typename T, typename queueT = std::list<T>>
class syncronized_queue {
 public:
  syncronized_queue(std::size_t limit = (std::size_t)-1) :
      max_size(limit < 1 ? 1 : limit) {}

  syncronized_queue(const syncronized_queue&) = delete;
  syncronized_queue& operator=(const syncronized_queue&) = delete;
  void put(const T& data);
  T get();

 private:
  std::size_t max_size;
  queueT queue;
  std::mutex my_mutex;
  std::condition_variable buffer_not_empty, buffer_not_full;
};

// For the producer
template <typename T, typename queueT>
void syncronized_queue<T, queueT>::put(const T& data) {
  std::unique_lock<std::mutex> guard{my_mutex};
  while (max_size == queue.size())
    buffer_not_full.wait(guard);
  queue.push_back(data);
  // notify that queue is not empty
  buffer_not_empty.notify_one();
}

// For the consumer
template <typename T, typename queueT>
T syncronized_queue<T, queueT>::get() {
  std::unique_lock<std::mutex> guard{my_mutex};
  while (queue.empty())
    buffer_not_empty.wait(guard);
  T result = queue.front();
  queue.pop_front();
  //notufy that the queue is not full
  buffer_not_full.notify_one();
  return result;
}
#endif // SYNC_QUEUE_H_
