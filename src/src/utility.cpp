#include "utility.h"
#include <random>

std::vector<std::string> split (const std::string& s, char delimiter) {
  std::string token;
  std::istringstream token_stream(s);
  std::vector<std::string> tokens;

  while (std::getline(token_stream, token, delimiter))
    tokens.push_back(token);

  return tokens;
}


double rand_double(int low, int high) {
  static std::default_random_engine re;
  std::uniform_real_distribution<double> unif(low, high);
  return unif(re);
}
