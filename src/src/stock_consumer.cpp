#include "stock_consumer.h"
#include <iostream>

stock_consumer::stock_consumer(syncronized_queue<std::string>& queue)
    : consumer(queue){}

void stock_consumer::config(const std::string& s, int i) {
  company = s;
  send(company + "%" + std::to_string(i));
}

void stock_consumer::consume(std::string& x) {
  auto t = split(x, '%');
  std::cout << "Company: " + company + ", value: " + t[0] + ", on " + t[1];
}
