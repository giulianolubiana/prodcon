#include "stock_producer.h"
#include <exception>
#include <iostream>

stock_producer::stock_producer(syncronized_queue<std::string>& queue)
    : producer(queue){}

void stock_producer::config() {
  auto t = split(read(), '%');
  s = t[0];
  i = std::atoi(t[1].c_str());
}

std::string stock_producer::produce() {
  std::this_thread::sleep_for(std::chrono::seconds(i));
  std::time_t now = std::time(0);
  std::string out;
  try {
    auto f = disp_table.at(s);
    out = std::to_string(f()) + "%" + ctime(&now);
  } catch (const std::out_of_range& e) {
    std::cerr << "There is no such company" << std::endl;
  }
  return out;
}


