#ifndef STOCK_CONSUMER_H_
#define STOCK_CONSUMER_H_

#include "consumer.h"
#include "syncronized_queue.h"
#include <string>

class stock_consumer : public consumer<std::string, syncronized_queue<std::string>> {
 public:
  stock_consumer(syncronized_queue<std::string>&);
  void config(const std::string&, int);

 private:
  void consume(std::string&) override;
  std::string company;
};

#endif //STOCK_CONSUMER_H_
