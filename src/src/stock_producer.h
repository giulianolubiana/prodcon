#ifndef STOCK_PRODUCER_H_
#define STOCK_PRODUCER_H_

#include <string>
#include <cstdlib>
#include <ctime>
#include <map>
#include "utility.h"
#include "producer.h"

class stock_producer :
    public producer<std::string, syncronized_queue<std::string>> {
 public:
  stock_producer(syncronized_queue<std::string>&);
  void config();
 private:
  std::string produce() override;

  int i;
  std::string s;

  std::map<const std::string , std::function<double(void)> > disp_table{
    {"X",[](void){ return rand_double(0, 10000);} },
    {"Y",[](void){ return rand_double(-1000, 2000);} },
    {"W",[](void){ return rand_double(-500, 500);} },
    {"Z",[](void){ return rand_double(1, 1000000);} } };


};

#endif // STOCK_PRODUCER_H_
