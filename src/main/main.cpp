#include "syncronized_queue.h"
#include "stock_producer.h"
#include "stock_consumer.h"
#include "thread_guard.h"
#include <iostream>
#include <thread>
#include <deque>
#include <cstdlib>

int main() {
  syncronized_queue<std::string> queue(10);
  stock_producer prod(queue);
  stock_consumer cons(queue);

  cons.config("X", 3);
  prod.config();

  auto production = [&](){ while (true) prod(); };
  std::thread t{production};
  thread_guard tg{t};

  while (true) cons();
}
