#include "gtest/gtest.h"
#include "syncronized_queue.h"
#include "stock_producer.h"
#include <string>
#include <chrono>


TEST(producer_test, read_parameter_from_buffer) {
  syncronized_queue<std::string> channel{10};
  stock_producer prod(channel);

  channel.put("X%1");
  prod.config();

  using namespace std::chrono;
  auto start = high_resolution_clock::now();
  prod();
  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<seconds>(stop - start);
  
  ASSERT_LE(1, duration.count());
  ASSERT_GE(2, duration.count());
}
