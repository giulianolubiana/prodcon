#include "gtest/gtest.h"
#include "syncronized_queue.h"
#include "stock_consumer.h"
#include <string>

TEST(consumer_test, creation) {
  syncronized_queue<std::string> channel;
  stock_consumer cons(channel);

  cons.config("X", 2);

  ASSERT_EQ(channel.get(), "X%2");
}

TEST(consumer_test, reading_from_channel) {
  syncronized_queue<std::string> channel;
  channel.put("1234.65%Sat Jan 8 20:07:41 2011");

  stock_consumer cons(channel);
  cons.config("X", 1);
  testing::internal::CaptureStdout();
  cons();
  std::string output = testing::internal::GetCapturedStdout();

  ASSERT_EQ(output, "Company: X, value: 1234.65, on Sat Jan 8 20:07:41 2011\n");
}
